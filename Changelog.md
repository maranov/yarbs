# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Removed

### Added

### Changed

## [0.4.1]

### Removed

### Added

### Changed
* Improved folder removal performance

## [0.4.0]

### Removed

### Added
* Added --ssh_server parameter to simplify sync over SSH
* Added support for removal of failed backup over SSH

### Changed
* BREAKING: Use dashes as command line param word separators (e.g. frm `--rsync_command` to `--rsync-command`)
* Sync is now done into a temporary folder (temporary folders are ignored for `--link-dest` selection)
* Root is no longer required

## [0.3.0]

### Removed

### Added
* Added an initial version with backup/prepare/sync and verbose/compression/bwlimit capabilities

### Changed
