#!/usr/bin/env python
# -*- coding: utf-8 -*-


from unittest import TestCase as _TestCase
from logging import basicConfig as _basicConfig, DEBUG as _DEBUG


_basicConfig(level=_DEBUG)


_debugging = False  # Turn on for interacitivity.


class test_yarbs(_TestCase):
    def test_yarbs(self):
        """
        Create a simple source dir and run backup three times.
        Not an automated test, needs manual review.
        """

        from os import mkdir, remove
        from os.path import join
        from shutil import rmtree
        from time import sleep
        from yarbs import backup

        source = 'test_yarbs_source'
        destination = 'test_yarbs_destination'

        mkdir(source)
        open(join(source, 'file1.txt'), 'w').close()
        open(join(source, 'file2.txt'), 'w').close()
        mkdir(destination)

        try:
            # First backup, full.
            backup(source, destination, backups_kept=2, verbose=True)
            sleep(1.1)
            if (_debugging): input('Waiting for continuation...')

            open(join(source, 'file3.txt'), 'w').close()
            # Second backup, file 1 and 2 linked, new file 3 added.
            backup(source, destination, backups_kept=2, verbose=True)
            sleep(1.1)
            if (_debugging): input('Waiting for continuation...')

            remove(join(source, 'file2.txt'))
            # Third backup, file 1 and 3 linked, file 2 removed,
            # the oldest (first) backup gets removed as well.
            backup(source, destination, backups_kept=2, verbose=True)
            sleep(1.1)
            if (_debugging): input('Waiting for continuation...')
        finally:
            rmtree(source)
            rmtree(destination)
